'use strict';

/**
 * @ngdoc function
 * @name tiendaFrontApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the tiendaFrontApp
 */
angular.module('tiendaFrontApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
