migrated to -> https://bitbucket.org/Tiendas/tienda-front

# tienda-front



## Build & development

# Install Grunt Globally
npm install -g grunt-cli

# Install Grunt Globally
npm install grunt --save-dev

# Install the rest of the dependencies
npm install

# Install bower globally
npm install -g bower

# Install bower locally
bower install

# Run grunt
grunt serve


## Testing

Running `grunt test` will run the unit tests with karma.